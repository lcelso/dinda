/****************************************************************
  FILE DESTINATIONS (RELATIVE TO ASSESTS FOLDER)
****************************************************************/
module.exports = {

    src: {
        main    : 'src',
        styles  : 'src/styles/*.styl',
        scripts : 'src/scripts/*.js'
    },
    dist: {
        styles: 'styles/',
        scripts: 'scripts/'
    },
    tasks: {
        jslint: 'jslint',
        jsmin: 'jsmin',
        jsconcat: 'jsconcat',
        jscs: 'jscs',
        js: 'js',
        styles: 'styles',
        cssmin: 'cssmin',
        cssconcat: 'cssconcat',
        html: 'html',
        browsersync: 'browsersync',
        zip: 'zip'
    },
    syncConfig: {
        files: ['styles/*.css', 'scripts/*.js', '/*.html'],
        server: {
            baseDir: './',
            index: 'index.html'
        }
    }
}
