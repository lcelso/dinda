(function($) {

    'use strict';

    var Carrousel = function(element, options) {
        this.$element = $(element);
        this.options = options;
        this.$elCarrousel = $('[data-carrousel="list"]');
        this.$item_width = $('[data-carrousel="list"] li').outerWidth(); 
        this.left_value = this.$item_width * (-1); 
        this.$buttonPrev = $('[data-carrousel="prev"]');
        this.$buttonNext = $('[data-carrousel="next"]');
        this.textNext = 'next';
        this.valLeft;
    };

    Carrousel.DEFAULTS = {}
 
    Carrousel.prototype.init = function() {
        this.getAjax();
    };

    Carrousel.prototype.getAjax = function() {
        var url = "http://demo8444515.mockable.io/dinda.json";
        $.getJSON(url, $.proxy(this, "loaded"));
    };

    Carrousel.prototype.loaded = function(data) {
        var html = [];
        var carrousel = null;

        $.each(data.products, function(key, val) {

            carrousel =  "<li class='carrousel-item' data-carrousel='item'>";
            carrousel += "<a href='"+val.link+"' target='_blank' title='"+val.title+"' class='product-image' style='background-image: url(" + val.image + ");'>";
            carrousel += "<span>"+val.title+"</span></a>";
            carrousel += "<div class='product-detail'><h3 class='product-title'><a href='"+val.link+"' rel='nofollow' target='_blank'>"+val.title+"</a></h3>";
            carrousel += "<p class='product-tag'>"+val.tag+"</p>";
            carrousel += "<p class='product-off'>"+val.off+" Off</p>";
            carrousel += "<p class='product-descount-price'>R$ "+val.descount+"</p>";
            carrousel += "<p class='product-installments'>em até "+val.installments+"x <span class='installments-price'>R$ "+val.installmentsPrice+"</span></p>";
            carrousel += "<p class='product-price'>R$ "+val.price+"</p></div></li>";

            html.push(carrousel);
        });

        this.$elCarrousel.append(html); 
        this.wrapCarrousel();
    };

    Carrousel.prototype.convert = function(valLeft, fn) {
        var newstr = valLeft.replace('-', '');
        (fn === 'next') ?
        this.valLeft = (parseInt(newstr) + ((this.$item_width) * 1.5)) :
        this.valLeft = (parseInt(newstr) * 1.5);
        
        return this.valLeft;
    };

    Carrousel.prototype.next = function() {
        var itemLeft = parseInt(this.$elCarrousel.css('left')) - (this.$item_width + 30);        
        this.convert(this.$elCarrousel.css('left'), this.textNext);

        (this.valLeft < this.$elCarrousel.width()) ? 
            this.animateItem(itemLeft, this.textNext) : 
            this.itemHide(this.$buttonNext, this.textNext);
    };

    Carrousel.prototype.prev = function() {
        var itemLeft = parseInt(this.$elCarrousel.css('left')) + (this.$item_width + 30);
        (this.convert(this.$elCarrousel.css('left')) > 0) ? 
            this.animateItem(itemLeft) : 
            this.itemHide(this.$buttonPrev);
    };

    Carrousel.prototype.animateItem = function(item, fn) {
        this.$elCarrousel.animate({left:  item}, 500);
        (fn === 'next') ? this.$buttonPrev.addClass('show') : this.$buttonNext.addClass('show');
    };    

    Carrousel.prototype.itemHide = function(el, fn) {
        (fn === 'next') ? el.removeClass('show').addClass('hide') : el.removeClass('show').addClass('hide');
    };

    Carrousel.prototype.animateHover = function(){
        $('[data-carrousel="next"]').animate({
                'right': '10px'
        }, 200);

        $('[data-carrousel="prev"]').animate({
                'left': '10px'
        }, 200);
    };

    Carrousel.prototype.animateOut = function(){
        $('[data-carrousel="next"]').animate({
                'right': '0'
        }, 200);

        $('[data-carrousel="prev"]').animate({
                'left': '0'
        }, 200);
    };

    Carrousel.prototype.wrapCarrousel = function() {
        var element = this.$elCarrousel.find('li');
        var items = element.length;
        var widthCarrousel = items * (element.width() + 30);

        this.$elCarrousel.css('width', widthCarrousel);
        this.$buttonPrev.addClass('hide');
    };

    $.fn.carrousel = function(option, args) {
        return this.each(function() {
            var $this = $(this);
            var data = $this.data('dna.carrousel');
            var options = $.extend({}, Carrousel.DEFAULTS, $this.data(), typeof option == 'object' && option);

            if (!data) {
                $this.data('dna.carrousel', (data = new Carrousel(this, options)));
            }

            if (typeof option == 'string') data[option](args);
        });
    };

    $(document).on('mouseover', '.carrousel-wrapper', function(e) {
        e.preventDefault();
        $(e.currentTarget).carrousel('animateHover');
    });

    $(document).on('mouseout', '.carrousel-wrapper', function(e) {
        e.preventDefault();
        $(e.currentTarget).carrousel('animateOut');
    });

    $(document).on('click', '[data-carrousel="next"]', function(e) {
        e.preventDefault();
        $(e.currentTarget).carrousel('next');
    });

    $(document).on('click', '[data-carrousel="prev"]', function(e) {
        e.preventDefault();
        $(e.currentTarget).carrousel('prev');
    });

    $(function() {
        $(document).carrousel('init');
    });

})(jQuery);