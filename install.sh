# !/bin/sh


##################
# Code # Color   #
##################
function print { echo  "\033[1;32m=> $1\033[0m"; }
function msg_checking { echo  "\033[1;32m=> $1 ✔\033[0m"; }
function msg_installing { echo  "\033[1;33m==> $1 [updating] ✔\033[0m"; }
function msg_install { echo  "\033[1;33m==> $1 [installing]: $ $2\033[0m"; }
function msg_ok { echo  "\033[1;32m==> $1 installed ✔\033[0m"; }
function msg { echo  "\033[0;32m$1\033[0m"; }
function msg_alert { echo "\033[1;31m✖ $1 ✖\033[0m"; }



msg '     ____  __           ___         '
msg '    / __ \/_/_    __ __/  /___ _    '
msg '   / / / / /  \  / / __  / __  /    '
msg '  / /_/ / / /\ \/ / /_/ / /_/  \__  '
msg '  \____/\/_/  \__/\____/\__,_/\__/  '
msg '                                    '
msg '									 '


##################
#     Config     #
##################
function init {
	folder
	dependecies
}

function folder {

	msg_install "Criando diretórios"
	mkdir -p src/{scripts,styles}
	msg_ok "OK"

}

function dependecies {
	msg_installing "Installing Dependencies... please wait."
	sudo npm install
	sudo npm install -g bower
	bower install
	msg_ok "OK"
}

init
